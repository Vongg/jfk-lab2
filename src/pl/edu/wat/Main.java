package pl.edu.wat;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.Parameter;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.stmt.BlockStmt;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;


import javax.tools.*;
import java.io.*;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
        final String fileName = "src\\Class.java";
        final String alteredFileName = "src\\ClassAltered.java";
        CompilationUnit cu;

        try (FileInputStream in = new FileInputStream(fileName)) {
            cu = JavaParser.parse(in);
        }


        for (MethodDeclaration method : cu.getChildNodesByType(MethodDeclaration.class)) {
            createTestMethod(method, cu);
        }

        cu.getClassByName("Class").get().setName("ClassAltered");

        try (FileWriter output = new FileWriter(new File(alteredFileName), false)) {
            output.write(cu.toString());
        }

        File[] files = {new File(alteredFileName)};
        String[] options = {"-d", "out//production//Synthesis"};

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
        try (StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null)) {
            Iterable<? extends JavaFileObject> compilationUnits =
                    fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
            compiler.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    Arrays.asList(options),
                    null,
                    compilationUnits).call();

            diagnostics.getDiagnostics().forEach(d -> System.out.println(d.getMessage(null)));
        }
    }

    private static void createTestMethod(MethodDeclaration method, CompilationUnit cu) {
        if (method.getType().toString().equals("void")) {
            return;
        }

        MethodDeclaration testMethod = new MethodDeclaration();
        NodeList<Parameter> parameters = new NodeList<Parameter>();

        for (Parameter p : method.getParameters()) {
            parameters.add(p);
        }
        Parameter result = new Parameter(method.getType(), "result");
        parameters.add(result);

        testMethod.setName(new SimpleName(method.getNameAsString() + "_Test"));
        testMethod.setParameters(parameters);
        testMethod.setModifiers(method.getModifiers());
        testMethod.setType("void");

        StringBuilder sb = new StringBuilder();

        for (Parameter p : method.getParameters()) {
            sb.append(p.getNameAsString());
            sb.append(",");
        }

        String p = sb.toString();
        p = p.substring(0, p.length() - 1);

        MethodCallExpr call = new MethodCallExpr(null, "assert");
        call.addArgument(method.getNameAsString() + "(" + p + ") == result");


        BlockStmt block = new BlockStmt();
        block.addStatement(0, call);

        testMethod.setBody(block);
        cu.getClassByName("Class").get().addMember(testMethod);
    }


    private static class Rewriter extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(MethodDeclaration n, Void arg) {
            String methodName = n.getNameAsString();

            if (n.getType().toString().equals("void")) {
                return;
            }

            BlockStmt block = GetMethodStmt(n);

            Expression call = JavaParser.parseExpression("log(\"" + n.getNameAsString() + "\")");
            block.addStatement(0, call);
        }
    }

    private static BlockStmt GetMethodStmt(MethodDeclaration method) {
        BlockStmt block;
        Optional<BlockStmt> body = method.getBody();
        if (!body.isPresent()) {
            block = new BlockStmt();
            method.setBody(block);
        } else {
            block = body.get();
        }

        return block;
    }
}
