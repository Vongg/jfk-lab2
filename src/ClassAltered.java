public final class ClassAltered {

    private static int sum(int a, int b) {
        return a + b;
    }

    public static int sub(int a, int b) {
        return a - b;
    }

    private static void p(String str) {
        System.out.println(str);
    }

    public static void main(String[] args) {
        System.out.println(sum(5, 10));
        System.out.println(sub(14, 5));
        p("tekst");
    }

    private static void sum_Test(int a, int b, int result) {
        assert(sum(a, b) == result);
    }

    public static void sub_Test(int a, int b, int result) {
        assert(sub(a, b) == result);
    }
}
